const express = require("express");

const app = express();


async function start = () =>
try {
  const Sequelize = require("sequelize");
  await const sequelize = new Sequelize("trello", "anna", "fusion", {
    dialect: "postgres"
  });
  app.listen(3005, (err:any) => {
  if (err) {
    console.log('Server cant start', err);
    process.exit(1);
  }
  
  console.log("Server has been started");
});